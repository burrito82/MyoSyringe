package de.root13.myosyringe;

import android.util.Log;

import eu.darken.myolib.BaseMyo;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.processor.emg.EmgData;
import eu.darken.myolib.processor.emg.EmgProcessor;
import eu.darken.myolib.processor.imu.ImuData;
import eu.darken.myolib.processor.imu.ImuProcessor;

/**
 * Created by Somebody on 22.10.2015.
 */
public class SyringeAnalyzer implements EmgProcessor.EmgDataListener, ImuProcessor.ImuDataListener, BaseMyo.ConnectionListener {
    private ISyringeListener mSyringeListener;

    private Myo mMyo;
    private ComputeLatestValue mComputeLatestValue = new ComputeLatestValue();
    private EmgProcessor mEmgProcessor;
    private ImuProcessor mImuProcessor;
    private long mLatestTimestamp;
    private GyroState mGyroState = GyroState.ARM_UNSTABLE;
    private float mLatestGyroMag;
    private long mGyroStabilizingBegin;
    private long mGyroStableBegin;
    private long mGyroUnstableBegin;
    private float m_fMaxAdmValue;
    private float m_fWaitingValueAverage;
    private float m_fValueAverage = 10.0f;
    private long m_uiSyringeBegin;
    private long m_uiSyringeEnd;
    private SyringeState m_eState = SyringeState.WAITING;

    public SyringeAnalyzer(ISyringeListener syringeListener) {
        mSyringeListener = syringeListener;
    }

    @Override
    public void onConnectionStateChanged(BaseMyo myo, BaseMyo.ConnectionState state) {
        Log.d(MyoSyringeActivity.TAG, "onConnectionStateChanged(...)");
        if (myo != null) {
            mMyo = (Myo) myo;
            if (state == BaseMyo.ConnectionState.CONNECTED || state == BaseMyo.ConnectionState.CONNECTING) {
                mEmgProcessor = new EmgProcessor();
                mEmgProcessor.addListener(this);
                mMyo.addProcessor(mEmgProcessor);
                mImuProcessor = new ImuProcessor();
                mImuProcessor.addListener(this);
                mMyo.addProcessor(mImuProcessor);
                mMyo.writeVibrate(MyoCmds.VibrateType.LONG, null);
            } else {
                mMyo.removeProcessor(mEmgProcessor);
                mMyo.removeProcessor(mImuProcessor);
                mMyo.writeVibrate(MyoCmds.VibrateType.SHORT, null);
                mMyo.writeVibrate(MyoCmds.VibrateType.SHORT, null);
                mMyo.writeVibrate(MyoCmds.VibrateType.SHORT, null);
            }
        }
    }

    private void setLatestTimestamp(final long timestamp) {
        mLatestTimestamp = timestamp;
    }

    public long getmLatestTimestamp() {
        return mLatestTimestamp;
    }

    @Override
    public void onNewEmgData(final EmgData emgData) {
        setLatestTimestamp(emgData.getTimestamp());
        addEmg(emgData.getData());
    }

    @Override
    public void onNewImuData(final ImuData imuData) {
        setLatestTimestamp(imuData.getTimeStamp());
        final double[] gyro = imuData.getGyroData();
        updateGyroscope((float) Math.sqrt(gyro[0] * gyro[0] + gyro[1] * gyro[1] + gyro[2] * gyro[2]));
    }

    private void addEmg(final byte[] emg) {
        mComputeLatestValue.AddEmg(emg);
        updateState();
    }

    private void updateGyroscope(float gyroMag) {
        mLatestGyroMag = gyroMag;

        final float fThreshold = 10.0f;
        if ((mGyroState != GyroState.ARM_UNSTABLE)
                && (gyroMag > fThreshold)) {
            Log.d(MyoSyringeActivity.TAG, "gyro now UNSTABLE");
            mGyroState = GyroState.ARM_UNSTABLE;
            mGyroUnstableBegin = mLatestTimestamp;
            publishPublicState();
            updateState();
            return;
        }
        if (mGyroState == GyroState.ARM_UNSTABLE
                && mLatestGyroMag < fThreshold
                && (mLatestTimestamp - mGyroUnstableBegin) > 1000) {
            Log.d(MyoSyringeActivity.TAG, "gyro now STABILIZING");
            mGyroState = GyroState.ARM_STABILIZING;
            mGyroStabilizingBegin = mLatestTimestamp;
            publishPublicState();
            updateState();
            return;
        }
        if (mGyroState == GyroState.ARM_STABILIZING
                && (mLatestGyroMag < fThreshold)
                && (mLatestTimestamp - mGyroStabilizingBegin) > 2000) {
            Log.d(MyoSyringeActivity.TAG, "gyro now STABLE");
            mGyroState = GyroState.ARM_STABLE;
            mGyroStableBegin = mLatestTimestamp;
            publishPublicState();
            updateState();
            return;
        }
    }

    public long getSyringeBegin() {
        return m_uiSyringeBegin;
    }

    public long getSyringeEnd() {
        return m_uiSyringeEnd;
    }

    private void updateState() {
        final float fValue = mComputeLatestValue.GetLatestValue();
        final float fBeginThreshold = 4.0f;
        final float fDiff = fValue - m_fValueAverage;
        final boolean bBeginThreshold = fDiff > fBeginThreshold;
        final boolean bStartMeasuring = bBeginThreshold
                && m_eState == SyringeState.WAITING
                && mGyroState == GyroState.ARM_STABLE
                && (mLatestTimestamp - m_uiSyringeBegin) > 1000 // ignore event duplicates
                && (mLatestTimestamp - m_uiSyringeEnd) > 5000; // leave some time between measurements

        final boolean bEndThreshold = fValue < 0.5f * m_fMaxAdmValue;
        final boolean bStopMeasuring = m_eState == SyringeState.ADMINISTRATION_IN_PROGRESS
                && (bEndThreshold
                || mGyroState != GyroState.ARM_STABLE)
                && (mLatestTimestamp - m_uiSyringeBegin) > 500;

        if (bStartMeasuring) {
            m_uiSyringeBegin = mLatestTimestamp;
            m_fMaxAdmValue = fValue;
            m_fWaitingValueAverage = m_fValueAverage;
            m_eState = SyringeState.ADMINISTRATION_IN_PROGRESS;
            Log.d(MyoSyringeActivity.TAG, "ADMINISTRATION_IN_PROGRESS");
            publishPublicState();
        }

        if (m_eState == SyringeState.ADMINISTRATION_IN_PROGRESS) {
            m_fMaxAdmValue = Math.max(m_fMaxAdmValue, fValue);
            publishPublicState();
        }

        if (bStopMeasuring) {
            m_uiSyringeEnd = mLatestTimestamp;
            m_eState = SyringeState.WAITING;
            m_fMaxAdmValue = 0.0f;
            publishPublicState();
            Log.d(MyoSyringeActivity.TAG, "WAITING");
            m_fValueAverage = m_fWaitingValueAverage;
        }

        if (mGyroState == GyroState.ARM_STABLE
                || mGyroState == GyroState.ARM_STABILIZING) {
            final float fAlpha = 0.95f;
            m_fValueAverage = fAlpha * m_fValueAverage + (1.0f - fAlpha) * fValue;
        }
    }

    private void publishPublicState() {
        AnalyzerState publicState = AnalyzerState.WAITING;
        switch (mGyroState) {
            case ARM_UNSTABLE:
                mSyringeListener.setState(AnalyzerState.WAITING);
                return;
            case ARM_STABILIZING:
                mSyringeListener.setState(AnalyzerState.STABILIZING);
                return;
            case ARM_STABLE:
                switch (m_eState) {
                    case WAITING:
                        if ((mLatestTimestamp - m_uiSyringeBegin) < 500) {
                            mSyringeListener.setState(AnalyzerState.ABORTED);
                            return;
                        }
                        if ((mLatestTimestamp - m_uiSyringeEnd) < 5000) {
                            mSyringeListener.setState(AnalyzerState.FINISHED);
                            return;
                        }
                        mSyringeListener.setState(AnalyzerState.STABLE);
                        break;
                    case ADMINISTRATION_IN_PROGRESS:
                        mSyringeListener.setState(AnalyzerState.ADMINISTRATION_IN_PROGRESS);
                        return;
                }
                break;
        }
    }

    private enum GyroState {
        ARM_UNSTABLE,
        ARM_STABILIZING,
        ARM_STABLE
    }

    private enum SyringeState {
        WAITING,
        ADMINISTRATION_IN_PROGRESS
    }

    public enum AnalyzerState {
        WAITING,
        STABILIZING,
        STABLE,
        ADMINISTRATION_IN_PROGRESS,
        FINISHED,
        ABORTED
    }

    public interface ISyringeListener {
        void setState(SyringeAnalyzer.AnalyzerState eAnalyzerState);
    }

    private class ComputeLatestValue {
        private final float[] data = new float[25];
        private int iBegin = 0;

        public void AddEmg(final byte[] emg) {
            float fNewValue = 0.0f;
            for (final byte b : emg) {
                fNewValue += Math.abs((float) (b));
            }
            data[iBegin++] = fNewValue;
            if (iBegin >= 25) {
                iBegin = 0;
            }
        }

        public float GetLatestValue() {
            float fRetVal = 0.0f;
            for (final float f : data) {
                fRetVal += f;
            }
            fRetVal /= 25.0f;
            return fRetVal;
        }
    }
}
