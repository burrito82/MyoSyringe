package de.root13.myosyringe;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import eu.darken.myolib.BaseMyo;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.MyoConnector;


public class MyoSyringeActivity extends AppCompatActivity implements SyringeAnalyzer.ISyringeListener {
    public static final String TAG = "MyoSyringe";
    @Bind(R.id.textViewInstructions)
    public TextView mTextViewInstructions;
    @Bind(R.id.editTextDrugDose)
    public EditText mEditTextDrugDose;
    @Bind(R.id.editTextAdministrationRate)
    public EditText mEditTextAdministrationRate;
    @Bind(R.id.progressBar)
    public ProgressBar mProgressBar;
    @Bind(R.id.ampel)
    public FrameLayout mAmpel;
    @Bind(R.id.textViewStatus)
    public TextView mTextViewStatus;
    SyringeAnalyzer.AnalyzerState mTestState = SyringeAnalyzer.AnalyzerState.WAITING;
    private SyringeAnalyzer mSyringeAnalyzer;
    private boolean mScanning = false;
    private Myo mMyo;
    private MyoConnector mMyoConnector;
    private int mDrugDose = 10;
    private float mAdmRate = 1.0f;
    private MyoConnector.ScannerCallback mScannerCallback = new MyoConnector.ScannerCallback() {
        @Override
        public void onScanFinished(List<Myo> myos) {
            if (!myos.isEmpty()) {
                mMyo = myos.get(0);
                mMyo.addConnectionListener(mSyringeAnalyzer);
                mMyo.connect();
                mMyo.setConnectionSpeed(BaseMyo.ConnectionSpeed.HIGH);
                mMyo.writeSleepMode(MyoCmds.SleepMode.NEVER, null);
                mMyo.writeMode(MyoCmds.EmgMode.FILTERED, MyoCmds.ImuMode.RAW, MyoCmds.ClassifierMode.DISABLED, null);
                mMyo.writeUnlock(MyoCmds.UnlockType.HOLD, null);
            } else {
                Log.d(TAG, "no myo found!");
            }
            if (mScanning) {
                mMyoConnector.scan(2000, mScannerCallback);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        mSyringeAnalyzer = new SyringeAnalyzer(this);
        mMyoConnector = new MyoConnector(this);
    }

    @Override
    protected void onStop() {
        ButterKnife.unbind(this);
        super.onStop();
    }

    @OnEditorAction(R.id.editTextDrugDose)
    public boolean changedDrugDose() {
        Log.d(TAG, "drug dose changed");
        String str = mEditTextDrugDose.getText().toString();
        try {
            mDrugDose = Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @OnEditorAction(R.id.editTextAdministrationRate)
    public boolean changedAdmRate() {
        Log.d(TAG, "administration rate changed");
        String str = mEditTextAdministrationRate.getText().toString();
        try {
            mAdmRate = Float.parseFloat(str);
            return true;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.ampel)
    public void AmpelClick() {
        setSyringeState(mTestState);
        switch (mTestState) {
            case WAITING:
                mTestState = SyringeAnalyzer.AnalyzerState.STABILIZING;
                break;
            case STABILIZING:
                mTestState = SyringeAnalyzer.AnalyzerState.STABLE;
                break;
            case STABLE:
                mTestState = SyringeAnalyzer.AnalyzerState.ADMINISTRATION_IN_PROGRESS;
                break;
            case ADMINISTRATION_IN_PROGRESS:
                mTestState = SyringeAnalyzer.AnalyzerState.FINISHED;
                break;
            case FINISHED:
                mTestState = SyringeAnalyzer.AnalyzerState.ABORTED;
                break;
            case ABORTED:
                mTestState = SyringeAnalyzer.AnalyzerState.WAITING;
                break;
        }
    }

    private void setSyringeState(SyringeAnalyzer.AnalyzerState eAnalyzerState) {
        switch (eAnalyzerState) {
            case WAITING:
                setInstruction(R.string.pleaseStabilize);
                updateProgress(0);
                mAmpel.setBackgroundColor(Color.RED);
                mTextViewStatus.setTextColor(Color.WHITE);
                break;
            case STABILIZING:
                setInstruction(R.string.goodStabilizing);
                mAmpel.setBackgroundColor(Color.YELLOW);
                mTextViewStatus.setTextColor(Color.BLACK);
                break;
            case STABLE:
                setInstruction(R.string.startDrugAdm);
                mAmpel.setBackgroundColor(Color.GREEN);
                mTextViewStatus.setTextColor(Color.WHITE);
                break;
            case ADMINISTRATION_IN_PROGRESS:
                float timeSinceStart = (mSyringeAnalyzer.getmLatestTimestamp() - mSyringeAnalyzer.getSyringeBegin()) / 1000.0f;
                final float fDoseGiven = timeSinceStart * mAdmRate;
                setInstruction(String.format("%d ml left", Math.max(0, (int) Math.ceil(mDrugDose - fDoseGiven))));
                mAmpel.setBackgroundColor(Color.GREEN);
                mTextViewStatus.setTextColor(Color.WHITE);
                updateProgress((int) Math.ceil(fDoseGiven / mDrugDose * 100.0f));
                break;
            case FINISHED:
                int timeNeeded = (int) (mSyringeAnalyzer.getSyringeEnd() - mSyringeAnalyzer.getSyringeBegin()) / 1000;
                setInstruction("Your time: " + timeNeeded + " sec. Perfect time would be: " + (int) (mDrugDose / mAdmRate) + " sec.");
                mAmpel.setBackgroundColor(Color.GREEN);
                mTextViewStatus.setTextColor(Color.WHITE);
                break;
            case ABORTED:
                setInstruction(R.string.abortMsg);
                updateProgress(0);
                mAmpel.setBackgroundColor(Color.RED);
                mTextViewStatus.setTextColor(Color.WHITE);
                break;
        }
    }

    private void setInstruction(String strInstruction) {
        mTextViewStatus.setText(strInstruction);
    }

    private void setInstruction(int id) {
        setInstruction(getString(id));
    }

    private void updateProgress(int iProgress) {
        mProgressBar.setProgress(iProgress);
    }

    @Override
    public void setState(final SyringeAnalyzer.AnalyzerState eAnalyzerState) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setSyringeState(eAnalyzerState);
            }
        });
    }

    @Override
    public void onResume() {
        mScanning = true;
        mMyoConnector.scan(2000, mScannerCallback);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mScanning = false;
        if (mMyo != null) {
            mMyo.removeConnectionListener(mSyringeAnalyzer);
            mMyo.setConnectionSpeed(BaseMyo.ConnectionSpeed.BALANCED);
            mMyo.writeSleepMode(MyoCmds.SleepMode.NORMAL, null);
            mMyo.writeMode(MyoCmds.EmgMode.NONE, MyoCmds.ImuMode.NONE, MyoCmds.ClassifierMode.DISABLED, null);
            mMyo.disconnect();
        }
        super.onPause();
    }
}
